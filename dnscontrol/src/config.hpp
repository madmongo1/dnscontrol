//
// Created by Richard Hodges on 21/09/2018.
//

#pragma once

#include <boost/beast.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/connect.hpp>
#include <wrapper/posix/filesystem.hpp>
#include <wrapper/posix/system_error.hpp>


namespace app
{
    namespace asio = boost::asio;
    namespace beast = boost::beast;

    using tcp = asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
    namespace ssl = asio::ssl;       // from <boost/asio/ssl.hpp>
    namespace http = beast::http;    // from <boost/beast/http.hpp>

    namespace system = wrapper::posix::system;
    namespace fs = wrapper::posix::filesystem;
}

