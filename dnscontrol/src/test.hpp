//
// Created by Richard Hodges on 21/09/2018.
//

#pragma once

#include "config.hpp"
#include "appsettings.hpp"

namespace app
{
    void test(asio::io_context &ioc, appsettings const &settings);
}


