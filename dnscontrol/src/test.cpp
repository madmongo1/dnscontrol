//
// Created by Richard Hodges on 21/09/2018.
//

#include "test.hpp"
#include <nlohmann/json.hpp>
#include <boost/format.hpp>
#include <ciso646>
#include "load_root_certificates.hpp"
#include <wrapper/core/print.hpp>

namespace app
{
    using wrapper::core::print;

    auto
    create_request(http::verb verb, boost::string_view host, boost::string_view uri, appsettings const &settings)
    -> http::request<http::string_body>
    {
        auto req = http::request<http::string_body>(verb, uri, 11);
        req.set(http::field::host, host);
        req.set(http::field::user_agent, BOOST_BEAST_VERSION_STRING);
        req.set(http::field::authorization, "sso-key " + settings.api_key + ":" + settings.api_secret);
        req.set(http::field::accept, "application/json");
        return req;
    }

    template<class Underlying>
    void set_tls_ext_hostname(ssl::stream<Underlying> &stream, std::string const &hostname)
    try
    {
        auto ok = ::SSL_set_tlsext_host_name(stream.native_handle(), hostname.c_str());
        if (not ok)
        {
            throw system::system_error(system::error_code(int(::ERR_get_error()), asio::error::get_ssl_category()));
        }
    }
    catch (...)
    {
        auto fmt = boost::format("set_tls_ext_hostname(%1%, %2%) - failed")
                   % stream.lowest_layer().remote_endpoint() % hostname;
        std::throw_with_nested(std::runtime_error(fmt.str()));
    }

    template<class Underlying>
    auto
    create_stream(ssl::stream<Underlying> &stream, std::string const &host)
    -> ssl::stream<tcp::socket> &
    {
        auto resolver = tcp::resolver(stream.get_io_context());
        auto endpoints = resolver.resolve(host, "https");
        asio::connect(stream.next_layer(), endpoints.begin(), endpoints.end());
        set_tls_ext_hostname(stream, host);
        stream.handshake(ssl::stream_base::client);
        return stream;
    }


    template<class Stream>
    auto read_response(Stream &stream) -> http::response<http::string_body>
    {
        // Declare a container to hold the response
        auto res = http::response<http::string_body>();

        // Receive the HTTP response
        auto b = boost::beast::flat_buffer();
        http::read(stream, b, res);
        return res;
    }

    void test(asio::io_context &ioc, appsettings const &settings)
    {
        using namespace std::string_literals;
        auto &&host = settings.api_host;

        auto ssl_context = ssl::context(ssl::context::sslv23_client);
        load_root_certificates(ssl_context);
        ssl::stream<tcp::socket> stream{ioc, ssl_context};
        create_stream(stream, host);
        http::write(stream, create_request(http::verb::get,
                                           host,
                                           "/v1/domains/" + settings.domain + "/records/A",
                                           settings));

        auto response = read_response(stream);
        if(response.result_int() == 200)
        {
            // Declare a container to hold the response
            using nlohmann::json;
            auto j = json::parse(response.body());

            for (auto &&entry : j)
            {
                print(std::cout,
                      entry.at("name").get_ref < std::string const& > (),
                " : ",
                entry.at("data").get_ref < std::string const& > ());
            }
        }
        else
        {
            throw std::runtime_error("invalid response:\n" + response.body());
        }
    }
}