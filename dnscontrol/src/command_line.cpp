//
// Created by Richard Hodges on 26/09/2018.
//

#include "command_line.hpp"

namespace app
{
    void append_short_option(std::string &shortopts, struct option const &opt)
    {
        if (opt.flag == 0 and std::isprint(opt.val))
        {
            shortopts += char(opt.val);
            switch (opt.has_arg)
            {
                default:
                case no_argument:
                    break;
                case required_argument:
                    shortopts += ':';
                    break;
                case optional_argument:
                    shortopts += "::";
                    break;
            }
        }
    }

}