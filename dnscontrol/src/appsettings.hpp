//
// Created by Richard Hodges on 21/09/2018.
//

#pragma once

#include "config.hpp"
#include <nlohmann/json.hpp>
#include <boost/noncopyable.hpp>
#include <wrapper/core/cstring_view.hpp>
#include <wrapper/core/print.hpp>

namespace app
{
    struct config_options
    {
        using cstring_view = wrapper::core::cstring_view;
        cstring_view config_option;
        cstring_view config_path_option;

        auto deduce_config_path() const -> fs::path;
    };

    struct appsettings
    : boost::noncopyable
    , config_options
    {
        using cstring_view = wrapper::core::cstring_view;

        appsettings(std::string program_name) : program_name(std::move(program_name)) {}

        template<class...Ts>
        void print_error(std::ostream& os, Ts&&...things)
        {
            wrapper::core::print(os, program_name, ": ", std::forward<Ts>(things)...);
        }

        int verbose_mode = 0;

        std::string program_name;
        cstring_view domain;

        std::string api_host;
        std::string api_key;
        std::string api_secret;

        void parse_config(nlohmann::json::json_pointer const &path, nlohmann::json const &config);

        bool verbose() const {
            return verbose_mode;
        }
    };
}


