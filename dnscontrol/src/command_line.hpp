//
// Created by Richard Hodges on 26/09/2018.
//

#pragma once

#include <getopt.h>
#include <string_view>
#include <vector>
#include <string>

namespace app
{
    struct extended_option :
    ::option
    {
        std::string_view description;
    };

    struct long_and_short_options
    {
        std::string short_options;
        std::vector<struct option> long_options;
    };

    void append_short_option(std::string &shortopts, struct option const &opt);

    template<std::size_t N>
    auto compute_options(extended_option const (&input)[N]) -> long_and_short_options
    {
        long_and_short_options result;
        for (auto &&eopt : input)
        {
            result.long_options.push_back(eopt);
            append_short_option(result.short_options, eopt);
        }
        result.long_options.push_back({});
        return result;
    }

}