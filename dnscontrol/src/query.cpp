//
// Created by Richard Hodges on 26/09/2018.
//

#include "query.hpp"
#include "command_line.hpp"
#include <wrapper/core/print.hpp>

namespace app
{

    namespace
    {
        [[noreturn]]
        void show_help(std::ostream& os, query_options const& opts)
        {
            using wrapper::core::print;
            print(os, opts.app_options.program_name, " query [--type=\"a\"] <name>");
            print(os, " queries a dns record");
            std::exit(1);
        }
    }

    int query(appsettings const &app_settings, int argc, char **argv)
    {
        auto opts = query_options(app_settings);

        static const extended_option options_def[] =
        {
        {"type", required_argument, 0, 't', "set the type of record to return - defaults to A"},
        {"help", no_argument,       0, '?', "show this help"},
        };
        int option_index = 0;
        auto options_set = compute_options(options_def);

        for (int c; (c = getopt_long(argc, argv, options_set.short_options.c_str(), options_set.long_options.data(),
                                     &option_index)) != -1;)
        {
            switch (c)
            {
                case 0:
                    break;
                case 't':
                    opts.type = ::optarg;
                    break;
                case '?':
                    show_help(std::cerr, opts);
                    break;
            }
        }

        if (optind == argc)
            throw std::invalid_argument("query: no name supplied");






        return 0;
    }

}