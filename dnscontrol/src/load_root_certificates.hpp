//
// Created by Richard Hodges on 21/09/2018.
//

#pragma once

#include "config.hpp"

namespace app
{
    void load_root_certificates(ssl::context &ctx);
}
