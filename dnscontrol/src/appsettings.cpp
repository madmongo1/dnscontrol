//
// Created by Richard Hodges on 21/09/2018.
//

#include "config.hpp"
#include "appsettings.hpp"
#include <boost/format.hpp>
#include <wrapper/posix/env.hpp>
#include <wrapper/core/print.hpp>

using namespace std::literals;

namespace app
{

    namespace
    {
        using namespace wrapper;

        auto deduce_path(core::cstring_view config, core::cstring_view config_path) -> fs::path
        {
            using posix::getenv;
            using std::invalid_argument;
            using std::runtime_error;
            using fs::path;

            if (not config && not config_path)
            {
                config_path = getenv("DNSCONTROL_CONFIG_PATH");
                if (not config_path)
                {
                    config = getenv("DNSCONTROL_CONFIG");
                    if (not config)
                    {
                        throw invalid_argument("options config and config_path are not set and no environment"
                                               " variable found for DNSCONTROL_CONFIG_PATH or DNSCONTROL_CONFIG");
                    }
                }
            }

            assert(config_path ^ config);

            if (not config_path)
            {
                auto home = getenv("HOME");
                if (!home)
                    throw runtime_error("HOME environment variable is not set");
                auto result = fs::path(home) / ".local" / "etc" / config.c_str() / "dnscontrol.config";
                return result;
            }
            else
            {
                return path(config_path);
            }

        }
    }

    auto config_options::deduce_config_path() const -> fs::path
    {
        return deduce_path(config_option, config_path_option);
    }

    void appsettings::parse_config(nlohmann::json::json_pointer const &path, nlohmann::json const &config)
    try
    {
        auto &&section = config.at(path);
        api_host = section.at("apiHost");
        api_key = section.at("apiKey");
        api_secret = section.at("secret");
    }
    catch (...)
    {
        auto message = core::print("appsettings::parse_config("s, path, ", ", config.dump(), ')');
        std::throw_with_nested(std::runtime_error(message));
    }

}