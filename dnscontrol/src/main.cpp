#include "config.hpp"
#include "test.hpp"
#include "query.hpp"
#include <exception>
#include <string>
#include <stdexcept>
#include <iostream>
#include <boost/program_options.hpp>
#include <nlohmann/json.hpp>
#include <wrapper/posix.hpp>
#include "appsettings.hpp"
#include <boost/optional.hpp>
#include "command_line.hpp"
#include <wrapper/core/print.hpp>

namespace posix = wrapper::posix;

namespace app
{
    int app_main(appsettings const &settings)
    {
        using namespace app;
        auto executor = asio::io_context();

        test(executor, settings);
        return 0;
    }


    struct diagnoser
    {
        void next(std::ostream &os, std::exception &e, std::size_t level)
        {
            try
            {
                std::rethrow_if_nested(e);
            }
            catch (...)
            {
                diagnose(os, std::current_exception(), level);
            }

        }

        void diagnose(std::ostream &os, std::exception_ptr ep, std::size_t level = 0)
        {
            using wrapper::core::print;
            auto indent = [&level]() -> std::string const &
            {
                thread_local static std::string buffer;
                buffer.assign(level, ' ');
                return buffer;
            };

            try
            {
                std::rethrow_exception(ep);
            }
            catch (std::invalid_argument &e)
            {
                print(os, indent(), "invalid argument: ", e.what());
            }
            catch (system::system_error &e)
            {
                print(os, indent(), "system error: ", e.what(), ", code=", e.code().value(),
                      ", category=", e.code().category().name());
                next(os, e, level + 1);
            }
            catch (std::runtime_error &e)
            {
                print(os, indent(), "runtime error: ", e.what());
                next(os, e, level + 1);
            }
            catch (std::logic_error &e)
            {
                print(os, indent(), "logic error: ", e.what());
                next(os, e, level + 1);
            }
            catch (std::exception &e)
            {
                print(os, indent(), "exception: ", e.what());
                next(os, e, level + 1);
            }
            catch (...)
            {
                print(os, indent(), "unknown exception");
            }
        }

    };

    void diagnose(std::ostream &os, std::exception_ptr ep = std::current_exception())
    {
        auto d = diagnoser();
        d.diagnose(os, ep, 0);
    }
}

namespace app
{
    [[noreturn]]
    void show_help(std::ostream &os)
    {
        os << "help" << std::endl;
        std::exit(1);
    }

    int add_record(appsettings const &settings, int argc, char **argv)
    {
        const char *sep = "";
        for (auto first = argv, last = first + argc; first != last; ++first)
        {
            std::cout << sep << *first;
            sep = " ";
        }
        std::cout << std::endl;
        return 0;
    }
}


int main(int argc, char **argv)
{
    namespace po = boost::program_options;
    using namespace app;
    using namespace std::literals;
    using wrapper::core::cstring_view;
    using wrapper::core::print;

    auto config_ptr = "/production"_json_pointer;
    auto settings = appsettings(fs::path(argv[0]).leaf().string());
    static const extended_option options_def[] =
    {
    /* These options set a flag. */
    {"verbose",     no_argument,       &settings.verbose_mode, 1,   "set verbose mode"},
    {"brief",       no_argument,       &settings.verbose_mode, 0,   "set brief mode"},
    /* These options don’t set a flag.
       We distinguish them by their indices. */
    {"debug",       no_argument,       0,                      'd', "use the ote path in the config json"},
    {"config",      required_argument, 0,                      'c', "which standard configuration file to use"},
    {"config_path", required_argument, 0,                      'p', "use a custom configuration file"},
    {"domain",      required_argument, 0,                      'm', "the domain to query"},
    {"help",        no_argument,       0,                      '?', "show this help"},

    };

    int option_index = 0;
    auto options_set = compute_options(options_def);

    for (int c; (c = getopt_long(argc, argv, options_set.short_options.c_str(), options_set.long_options.data(),
                                 &option_index)) != -1;)
    {
        switch (c)
        {
            case 0:
                break;
            case 'd':
                config_ptr = "/ote"_json_pointer;
                break;
            case 'c':
                settings.config_option = ::optarg;
                break;
            case 'p':
                settings.config_path_option = ::optarg;
                break;
            case 'm':
                settings.domain = ::optarg;
                break;
            case '?':
                show_help(std::cerr);
                break;
        }
    }

    try
    {
        if (settings.domain.invalid())
            throw std::invalid_argument("option --domain must be set");

        using nlohmann::json;
        auto config_file = settings.deduce_config_path();
        if (settings.verbose())
        {
            print(std::cout, "config_file = ", config_file);
            print(std::cout, "config_ptr = ", config_ptr);
        }
        auto config_json = json::parse(byte_stream(mmap(posix::open(config_file))));
        settings.parse_config(config_ptr, config_json);

        auto command = cstring_view("test");

        if (::optind < argc)
        {
            command = argv[::optind];
            if (command == "test")
                return app_main(settings);
            else if (command == "query")
                return query(settings, argc - optind, argv + optind);
            else if (command == "add-record")
                return add_record(settings, argc - optind, argv + optind);
            throw std::runtime_error("invalid command: " + command);
        }
        else
        {
            app_main(settings);
        }
    }
    catch (std::exception &e)
    {
        settings.print_error(std::cerr, e.what());
        if (settings.verbose())
        {
            diagnose(std::cerr);
        }
        return 4;
    }

    return 0;
}