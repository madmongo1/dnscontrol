//
// Created by Richard Hodges on 26/09/2018.
//

#pragma once

#include "config.hpp"
#include "appsettings.hpp"
#include <wrapper/core/cstring_view.hpp>


namespace app
{
    struct query_options
    {
        using cstring_view = wrapper::core::cstring_view;

        query_options(appsettings const& app_options) : app_options(app_options) {}

        appsettings const& app_options;

        cstring_view type = cstring_view("A");
        std::string name;
    };

    int query(appsettings const& app_settings, int argc, char **argv);
}

