
Project(DnsControl)

hunter_add_package(sugar)
find_package(sugar CONFIG REQUIRED)

sugar_include(src)
add_executable(dnscontrol ${SRC_FILES})
include(AddOpenSSL)
AddOpenSSL(dnscontrol)
include(AddBoost)
AddBoost(dnscontrol COMPONENTS filesystem program_options system thread)
hunter_add_package(nlohmann_json)
find_package(nlohmann_json REQUIRED)
target_link_libraries(dnscontrol PUBLIC nlohmann_json::nlohmann_json)
target_link_libraries(dnscontrol PUBLIC Wrapper::Posix)
