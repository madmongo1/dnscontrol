if (DEFINED DnsControl_SetCompileOptions_Included)
    return()
endif ()
set(DnsControl_SetCompileOptions_Included 1)

include(CheckCXXCompilerFlag)

macro(WrapperTestOption option var category)
    check_cxx_compiler_flag("${option}" ${var})
    set(${var} "${${var}}" CACHE INTERNAL "compiler supports ${option} flag")
    message(STATUS "compiler supports ${option} flag: ${${var}}")
    list(APPEND ${category} ${option})
    list(REMOVE_DUPLICATES ${category})
    set(${category} "${${category}}" CACHE INTERNAL "options of category ${category}")
endmacro()

WrapperTestOption(-Wall COMPILER_SUPPORTS_WALL ALL_WARNING_FLAGS)
WrapperTestOption(-Wextra COMPILER_SUPPORTS_WEXTRA ALL_WARNING_FLAGS)
WrapperTestOption(-pedantic COMPILER_SUPPORTS_PEDANTIC ALL_WARNING_FLAGS)

macro(SetCompileOptions SCO_target)

    if (CMAKE_CXX_COMPILER_ID MATCHES "Clang")
        target_compile_options(${SCO_target} PRIVATE -Wdocumentation)
    endif ()

    if (CMAKE_BUILD_TYPE STREQUAL "Debug")
        target_compile_options(${SCO_target} PRIVATE ${ALL_WARNING_FLAGS})
    endif ()

endmacro()
