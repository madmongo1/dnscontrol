

if (NOT DEFINED CMAKE_TOOLCHAIN_FILE)

    set(DEFAULT_POLLY_PATH "${CMAKE_SOURCE_DIR}/polly")

    if (NOT DEFINED POLLY_ROOT)
        set(POLLY_ROOT "${DEFAULT_POLLY_PATH}")
    endif ()

    message(STATUS "[project] POLLY_ROOT: ${POLLY_ROOT}")


    if (APPLE)
        set(CMAKE_TOOLCHAIN_FILE ${POLLY_ROOT}/cxx17.cmake)
    elseif (WINDOWS)
        set(CMAKE_TOOLCHAIN_FILE ${POLLY_ROOT}/vs-15-2017-win64-cxx17.cmake)
    else ()
        set(CMAKE_TOOLCHAIN_FILE ${POLLY_ROOT}/cxx17.cmake)
    endif ()

    if (NOT EXISTS "${CMAKE_TOOLCHAIN_FILE}")
        if (POLLY_ROOT STREQUAL DEFAULT_POLLY_PATH)
            message(STATUS "[toolchain] initialising submodules ${CMAKE_SOURCE_DIR}")
            message(STATUS "[toolchain] Updating polly submodule")
            execute_process(WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}" COMMAND git submodule update)
        endif ()
    endif ()


    if (NOT EXISTS ${CMAKE_TOOLCHAIN_FILE})
        execute_process(WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                COMMAND git submodule init)
        execute_process(WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                COMMAND git submodule update polly)
    endif ()

endif ()

message(STATUS "[project] CMAKE_TOOLCHAIN_FILE: ${CMAKE_TOOLCHAIN_FILE}")
include(${CMAKE_TOOLCHAIN_FILE})
